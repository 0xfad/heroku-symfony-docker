#!/bin/ash

set -xe

# ---------------------------------------------------------------------------------------------------- #
# VARIABLES
# ---------------------------------------------------------------------------------------------------- #
CURRENT_USER="$(whoami)"

# ---------------------------------------------------------------------------------------------------- #
# NGINX
# ---------------------------------------------------------------------------------------------------- #
if [ -f /var/www/nginx.conf ]; then
  mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.original
  mv /var/www/nginx.conf /etc/nginx/nginx.conf
fi

if [ -f /etc/nginx/nginx.conf.original ]; then
  mv /etc/nginx/nginx.conf.original /etc/nginx/nginx.conf
fi

sed -i -E "s/__USER__/${CURRENT_USER}/g" /etc/nginx/nginx.conf
sed -i -E "s/__PORT__/${PORT:-8080}/g" /etc/nginx/nginx.conf

mkdir -p /var/tmp/nginx
# ---------------------------------------------------------------------------------------------------- #


# ---------------------------------------------------------------------------------------------------- #
# PHP-FPM
# ---------------------------------------------------------------------------------------------------- #
if [ "$CURRENT_USER" == "root" ]; then
  CURRENT_USER="nginx"
fi

if [ "$APP_ENV" == "prod" ]; then
  sed -i -E "s/^opcache.enable = (.*)/opcache.enable = 1/" /etc/php7/php.ini
else
  sed -i -E "s/^opcache.enable = (.*)/opcache.enable = 0/" /etc/php7/php.ini
fi

sed -i -E "s/^listen.owner = .*/listen.owner = ${CURRENT_USER}/" /etc/php7/php-fpm.conf
sed -i -E "s/^user = .*/user = ${CURRENT_USER}/" /etc/php7/php-fpm.conf
sed -i -E "s/^group = (.*)/;group = \1/" /etc/php7/php-fpm.conf
# ---------------------------------------------------------------------------------------------------- #

supervisord -c /etc/supervisord.conf
