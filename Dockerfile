FROM alpine:3.12

ENV TZ=Europe/Rome

RUN apk --update --no-cache add \
  php7 \
  php7-fpm \
  php7-common \
  php7-ctype \
  php7-curl \
  php7-fileinfo \
  php7-mbstring \
  php7-zip \
  php7-dom \
  php7-json \
  php7-xml \
  php7-phar \
  php7-tokenizer \
  php7-openssl \
  php7-iconv \
  php7-opcache \
  php7-pgsql \
  php7-pdo_pgsql \
  php7-session \
  php7-pdo \
  argon2-libs \
  wget \
  curl \
  git \
  supervisor \
  nginx \
  tzdata \
  openssl \
  && \
  curl -sS https://getcomposer.org/installer | php7 -- --install-dir=/usr/bin --filename=composer \
  && rm -rf /var/www && mkdir -p /var/www \
  && cp /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \
  && rm -rf /var/cache/apk/* /root/.composer/cache/* \
  && adduser --no-create-home -D dyno \
  && chown dyno:dyno /var/www

# fix work iconv library with alphine
RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted gnu-libiconv
ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

COPY --chown=dyno:dyno nginx.conf /etc/nginx/nginx.conf
COPY --chown=dyno:dyno php.ini /etc/php7/php.ini
COPY --chown=dyno:dyno php-fpm.conf /etc/php7/php-fpm.conf
COPY --chown=dyno:dyno supervisord.conf /etc/supervisord.conf
COPY --chown=dyno:dyno entrypoint.sh /entrypoint.sh

RUN \
  chmod a+x /entrypoint.sh && \
  chmod -R a+rwx /etc/nginx && \
  chmod -R a+rwx /etc/php7 && \
  chmod -R a+rwx /var/log && \
  chmod -R a+rwx /var/lib/nginx && \
  chmod -R a+rwx /var/www

# USER dyno

WORKDIR /var/www

ENV APP_ENV dev
EXPOSE 8080

ONBUILD COPY . /var/www
ONBUILD RUN \
  if [ -f "composer.json" ]; then \
    composer install --no-interaction --no-cache --no-progress --quiet || : \
  ; fi

CMD ["/entrypoint.sh"]
